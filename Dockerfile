FROM docker.io/parrotsec/core:latest
MAINTAINER Richard Armuelles (alexanderarmuelles@riseup.net)
ENV DEBIAN_FRONTEND noninteractive
ENV VERSION 2.29-4.11

# Install components
RUN apt-get update; apt-get -y dist-upgrade;apt-get -y --no-install-recommends install parrot-menu;apt-get -y install bettercap bettercap-ui bettercap-caplets; rm -rf /var/lib/apt/lists/*

COPY bettercap-start /bettercap-start

ENTRYPOINT /bettercap-start $@
